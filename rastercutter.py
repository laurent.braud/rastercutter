#! /usr/bin/python3
# -*- coding: utf-8 -*-
#******************************************************************************
#  Author : Laurent Braud
#
#  Cuts a netcdf raster along polygons and returns average for each polygon in a
#  csv file. Pixels are weighted by area contained in the polygon.
#
#  Takes two inputs :
#  - A netcdf file containing exactly 4 variables, including 'time', 'lon', 'lat'
#    and the parameter. For best performance, the raster should be clipped to
#    the extent of the shapefile (or slightly larger).
#  - A shape file (.shp) containing exactly one layer of polygons.
#
#******************************************************************************


# disaggregation factor for calculating polygon area in raster
DISAGGREG = 50
# default fields in shp files
DEFAULT_NAMEFIELD = 'name'
DEFAULT_IDFIELD = 'OBJECTID'

import sys
from netCDF4 import Dataset, num2date
import gdal, ogr, osr
import numpy as np
import pandas as pd


SHPDRIVER = ogr.GetDriverByName('ESRI Shapefile')
MEMDRIVER = gdal.GetDriverByName('MEM')

shpfile = '/home/laurent/shp/sen_admbnda_adm1_1m_gov_ocha_04082017.shp'
ncfile = '/home/laurent/netcdf/pr_senegal.nc'


def newRaster(x0, xn, xres, y0, yn, yres):
    outRaster = MEMDRIVER.Create('', int((xn-x0)/xres), int((yn-y0)/yres), 1, gdal.GDT_Float64)
    outRaster.SetGeoTransform((x0,xres,0,y0,0,yres))
    outRasterSRS = osr.SpatialReference()
    outRasterSRS.ImportFromEPSG(4326)
    outRaster.SetProjection(outRasterSRS.ExportToWkt())
    return outRaster


def polygonize(ncfile, shpfile, param=None, nameField=DEFAULT_NAMEFIELD, idField=DEFAULT_IDFIELD):
    ds = Dataset(ncfile)

    # find parameter name
    if not param :
        if len(ds.variables) != 4:
            print("Too many parameters in netcdf file.")
            sys.exit(1)
        for v in ds.variables:
            if v not in [ 'time', 'lon', 'lat' ]:
                param = str(v)
                break
    print("Extracting variable {}\n{}".format(v, ds[v]))

    # builds calendar
    cal, units= ds['time'].calendar, ds['time'].units
    dates = map(lambda t:num2date(t, calendar=cal, units=units)._to_real_datetime().date(), ds['time'][:])
    df = pd.DataFrame(index=dates)
    df.index.name = 'date'

    # get raster extent
    x0, xn, xres = np.min(ds['lon']), np.max(ds['lon']), ds['lon'][1]-ds['lon'][0]
    y0, yn, yres = np.min(ds['lat']), np.max(ds['lat']), ds['lat'][1]-ds['lat'][0]
    x0 -= xres/2
    y0 -= yres / 2
    xn += xres / 2
    yn += yres / 2

    large_raster = newRaster(x0, xn, xres/DISAGGREG, y0, yn, yres/DISAGGREG)
    weights = newRaster(x0, xn, xres, y0, yn, yres)

    # netcdf values
    dsa = ds[param][:]

    dataSource = SHPDRIVER.Open(shpfile, 0) # 0 means read-only. 1 means writeable.
    layer = dataSource.GetLayer()
    for f in layer :
        print(f.GetField(nameField))
        where = '{}={}'.format(idField, f.GetField(idField))
        options = gdal.RasterizeOptions(allTouched=True, burnValues=1, where=where)

        gdal.Rasterize(large_raster, shpfile, options=options)
        gdal.Warp(weights, large_raster, options=gdal.WarpOptions(resampleAlg='average'))

        # weighted average
        a = weights.GetRasterBand(1).ReadAsArray()
        res = np.sum(np.multiply(dsa, a), axis=(1,2)) / np.sum(a)
        df[f.GetField(nameField)] = res

    return df


def usage():
    print("  Usage :\n"
          "{} [-n nameField] [-i idField] in.nc in.shp out.csv"
          "\n\n"
          "  nameField : name field in shapefile (default '{}')\n"
          "  idField   : id number field in shapefile (default '{}')"
          .format(sys.argv[0], DEFAULT_NAMEFIELD, DEFAULT_IDFIELD))
    sys.exit(1)


if __name__ == '__main__':
    nameField, idField = DEFAULT_NAMEFIELD, DEFAULT_IDFIELD

    if len(sys.argv) <= 1:
        usage()
        
    i = 1
    while sys.argv[i][0] == '-':
        if sys.argv[i][1] == 'n':
            nameField = sys.argv[i+1]
        elif sys.argv[i][1] == 'i':
            idField = sys.argv[i+1]
        else:
            usage()
        i += 2

    if len(sys.argv)-i != 3:
        usage()
    ncfile, shpfile, csvfile = sys.argv[i:]

    df = polygonize(ncfile, shpfile, nameField=nameField, idField=idField)
    df.to_csv(csvfile)
