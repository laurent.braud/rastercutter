Cuts a netcdf raster along polygons and returns average for each polygon in a
csv file. Pixels are weighted by area contained in the polygon.

Takes two inputs :
 - A netcdf file containing exactly 4 variables, including 'time', 'lon', 'lat'
   and the parameter. For best performance, the raster should be clipped to
   the extent of the shapefile (or slightly larger).
 - A shape file (.shp) containing exactly one layer of polygons.

Usage :
```
    rastercutter.py [-n nameField] [-i idField] in.nc in.shp out.csv
```
where 
 - nameField : name field in shapefile (default admin1Name)
 - idField   : id number field in shapefile (default OBJECTID)